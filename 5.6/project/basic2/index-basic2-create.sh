curl -XPUT 'localhost:9200/basic2?pretty' -H 'Content-Type: application/json' -d'
{
    "settings" : {
        "index" : {
            "number_of_shards" : 3,
            "number_of_replicas" : 2
        }
    },
    "mappings":{
    	"fixtureSet1Creature":{
	    	"properties":{
	    		"kind":{
	    			"type":"text"
	    		},
		    	"body":{
			    	"properties":{
				    	"arms":{ "type":"long" },
					    "head":{ "type":"long" },
					    "legs":{ "type":"long" },
					    "tail":{ "type":"long" }
					}
				}
			}
		}
	}
}'