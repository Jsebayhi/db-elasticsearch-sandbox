curl -XPUT 'http://localhost:9200/basic2/fixtureSet1Creature/1?pretty=1 ' -d ' 
  { 
    "kind": "human",
    "body": {
      "head": 1,
      "trunc": 1,
      "legs": 2,
      "arms": 2,
      "tail": 0
    }
  }
'
curl -XPUT 'http://localhost:9200/basic2/fixtureSet1Creature/2?pretty=1 ' -d ' 
  { 
    "kind": "dog",
    "body": {
      "head": 1,
      "trunc": 1,
      "legs": 2,
      "arms": 2,
      "tail": 1
    }
  }
'
curl -XPUT 'http://localhost:9200/basic2/fixtureSet1Creature/3?pretty=1 ' -d ' 
  { 
    "kind": "cat",
    "body": {
      "head": 1,
      "trunc": 1,
      "legs": 2,
      "arms": 2,
      "tail": 1
    }
  }
'