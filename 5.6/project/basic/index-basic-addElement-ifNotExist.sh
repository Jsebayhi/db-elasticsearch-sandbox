# basic is the already existing index name
# typeA is the type we have choosen for this document
# 1 is the document id

curl -XPUT 'localhost:9200/basic/typeA/1?op_type=create&pretty' -H 'Content-Type: application/json' -d'
{
    "user" : "kimchy2",
    "post_date" : "2009-11-15T14:12:12",
    "message" : "trying out Elasticsearch"
}
'
echo